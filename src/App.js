import logo from './assets/img/portfolio/me2.png';
import menu1 from './assets/img/portfolio/1.png';
import menu2 from './assets/img/portfolio/2.png';
import menu3 from './assets/img/portfolio/3.png';
import menu4 from './assets/img/portfolio/4.png';
import menu5 from './assets/img/portfolio/5.png';
import menu6 from './assets/img/portfolio/6.png';
import menu7 from './assets/img/portfolio/7.png';
import menu8 from './assets/img/portfolio/8.png';
import menu9 from './assets/img/portfolio/9.png';

import p1 from './assets/img/portfolio/p1.png';
import p2 from './assets/img/portfolio/p2.png';
import p3 from './assets/img/portfolio/p3.png';
import p4 from './assets/img/portfolio/p4.png';
import p5 from './assets/img/portfolio/p5.png';
import p6 from './assets/img/portfolio/p6.png';
import p7 from './assets/img/portfolio/p7.png';
import p8 from './assets/img/portfolio/p8.png';
import p9 from './assets/img/portfolio/p9.png';

import s1 from './assets/img/portfolio/s1.svg';
import s2 from './assets/img/portfolio/s2.png';
import s3 from './assets/img/portfolio/s3.svg';
import s4 from './assets/img/portfolio/s4.svg';
import s5 from './assets/img/portfolio/s5.svg';
import s6 from './assets/img/portfolio/s6.svg';
import s7 from './assets/img/portfolio/s7.svg';
import s8 from './assets/img/portfolio/s8.svg';
import s9 from './assets/img/portfolio/s9.svg';
import s10 from './assets/img/portfolio/s10.svg';
import s11 from './assets/img/portfolio/s11.svg';
import s12 from './assets/img/portfolio/s12.svg';
import s13 from './assets/img/portfolio/s13.svg';


import React, { useRef, useState } from 'react';
import './App.css';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';


function App() {
  const modalRef = useRef(null);

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    phone: '',
    message: ''
  });
  
  const [successMessage, setSuccessMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);

  const handleChange = (e) => {
    const { id, value } = e.target;
    setFormData(prevState => ({
      ...prevState,
      [id]: value
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setSuccessMessage(false);
    setErrorMessage(false);
  
    try {
      const response = await fetch('https://script.google.com/macros/s/AKfycbx1FyxXb93vosXkuHUsrfsYcROyCt-q7LG9049RlQcm4PNoxKfRZmi0oX7Tqh0xeyMrow/exec', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'text/plain'
        },
        body: JSON.stringify({
          name: formData.name,
          email: formData.email,
          phone: formData.phone,
          message: formData.message
        })
      });
  
      if (response.ok) {
        setSuccessMessage(true);
      } else {
        throw new Error('Network response was not ok');
      }
    } catch (error) {
      setErrorMessage(true);
    }
  };
  
  
  
  

  return (
    <div className="App">
      {/* <header className="App-header"> */}
      <html lang="en">
        <head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <meta name="description" content="" />
          <meta name="author" content="" />
          <title>Portfolio Anunkhaporn</title>

          <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />

          <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossOrigin="anonymous"></script>

          <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
          <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />

          <link href="css/styles.css" rel="stylesheet" />
        </head>
        <body id="page-top">
          {/* <!-- Navigation--> */}

          <nav className="navbar navbar-expand-lg text-uppercase fixed-top" style={{ backgroundColor: '#fec8dd' }} id="mainNav">
            <div className="container">
              <a className="navbar-brand" href="#page-top">Portfolio Anunkhaporn</a>
              <button className="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i className="fas fa-bars"></i>
              </button>
              <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav ms-auto">
                  <li className="nav-item mx-0 mx-lg-1"><a className="nav-link py-3 px-0 px-lg-3 rounded" href="#skill">Skill</a></li>
                  {/* <li className="nav-item mx-0 mx-lg-1"><a className="nav-link py-3 px-0 px-lg-3 rounded" href="#about">About</a></li> */}
                  <li className="nav-item mx-0 mx-lg-1"><a className="nav-link py-3 px-0 px-lg-3 rounded" href="#portfolio">Portfolio</a></li>
                  <li className="nav-item mx-0 mx-lg-1"><a className="nav-link py-3 px-0 px-lg-3 rounded" href="#contact">Contact</a></li>
                </ul>
              </div>
            </div>
          </nav>
          {/* <!-- Masthead--> */}
          <header className="masthead text-white text-center" style={{ backgroundColor: '#fec8dd' }}  >
            <div className="container d-flex align-items-center flex-column">
              {/* <!-- Masthead Avatar Image--> */}
              <img className="masthead-avatar mb-5" src={logo} alt="..." />
              {/* <!-- Masthead Heading--> */}
              <h1 className="masthead-heading text-uppercase mb-0">Anunkhaporn</h1>
              {/* <!-- Icon Divider--> */}
              <div className="divider-custom divider-light">
                <div className="divider-custom-line"></div>
                <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                <div className="divider-custom-line"></div>
              </div>
              {/* <!-- Masthead Subheading--> */}
              <p className="masthead-subheading font-weight-light mb-0">Front-End Developer</p>
              <p style={{ textAlign: 'left', margin: '2px', textIndent: '5%'}}>I have experience working as a front-end developer primarily using Vue.js and Angular. I also have experience managing a front-end team. I am eager to gain knowledge and experience in new programming languages or in the backend development field. My aspiration is to become a full stack developer, capable of working on both the front-end and back-end in the future.</p>
              {/* <!-- About Section Button--> */}
              <div className="text-center mt-4">
              <a className="btn btn-xl btn-outline-light" href={`${process.env.PUBLIC_URL}/Anunkhaporn_Resume.pdf`} download="Anunkhaporn_Resume.pdf">
                  <i className="fas fa-download me-2"></i>
                  Download Resume
                </a>
              </div>
            </div>
          </header>

          {/* <!-- Skill Section--> */}
          <section className="page-section portfolio" id="skill">
            <div className="container">
              {/* <!-- Portfolio Section Heading--> */}
              <h2 className="page-section-heading text-center text-uppercase text-secondary mb-0">Skill</h2>
              {/* <!-- Icon Divider--> */}
              <div className="divider-custom">
                <div className="divider-custom-line"></div>
                <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                <div className="divider-custom-line"></div>
              </div>
              {/* <!-- Portfolio Grid Items--> */}
              <div className="row justify-content-center">
                {/* <!-- Portfolio Item 1--> */}
                <div className="col-md-6 col-lg-4 mb-5">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal7">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu7} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 2--> */}
                <div className="col-md-6 col-lg-4 mb-5">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal8">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu8} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 3--> */}
                <div className="col-md-6 col-lg-4 mb-5">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal9">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu9} alt="..." />
                  </div>
                </div>

              </div>
            </div>
          </section>
          
          {/* <!-- About Section--> */}
          {/* <section className="page-section text-white mb-0" id="about" style={{ backgroundColor: '#9ea1d4' }} >
            <div className="container">
              <!-- About Section Heading-->
              <h2 className="page-section-heading text-center text-uppercase text-white">About</h2>
              <!-- Icon Divider-->
              <div className="divider-custom divider-light">
                <div className="divider-custom-line"></div>
                <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                <div className="divider-custom-line"></div>
              </div>
              <!-- About Section Content-->
              <div className="row">
                <div className="col-lg-4 ms-auto"><p className="lead">Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional SASS stylesheets for easy customization.</p></div>
                <div className="col-lg-4 me-auto"><p className="lead">You can create your own custom avatar for the masthead, change the icon in the dividers, and add your email address to the contact form to make it fully functional!</p></div>
              </div>
              
            </div>
          </section> */}
          
          {/* <!-- Portfolio Section--> */}
          <section className="page-section portfolio" id="portfolio" style={{ backgroundColor: '#9ea1d4' }} >
            <div className="container">
              {/* <!-- Portfolio Section Heading--> */}
              <h2 className="page-section-heading text-center text-uppercase text-white">Portfolio</h2>
              {/* <!-- Icon Divider--> */}
              <div className="divider-custom divider-light">
                <div className="divider-custom-line"></div>
                <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                <div className="divider-custom-line"></div>
              </div>
              {/* <!-- Portfolio Grid Items--> */}
              <div className="row justify-content-center">
                {/* <!-- Portfolio Item 1--> */}
                <div className="col-md-6 col-lg-4 mb-5">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal1">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu1} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 2--> */}
                <div className="col-md-6 col-lg-4 mb-5">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal2">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu2} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 3--> */}
                <div className="col-md-6 col-lg-4 mb-5">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal3">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu3} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 4--> */}
                <div className="col-md-6 col-lg-4 mb-5 mb-lg-0">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal4">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu4} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 5--> */}
                <div className="col-md-6 col-lg-4 mb-5 mb-md-0">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal5">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu5} alt="..." />
                  </div>
                </div>
                {/* <!-- Portfolio Item 6--> */}
                <div className="col-md-6 col-lg-4">
                  <div className="portfolio-item mx-auto" data-bs-toggle="modal" data-bs-target="#portfolioModal6">
                    <div className="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                      <div className="portfolio-item-caption-content text-center text-white"><i className="fas fa-plus fa-3x"></i></div>
                    </div>
                    <img className="img-fluid" src={menu6} alt="..." />
                  </div>
                </div>
              </div>
            </div>
          </section>
          
          {/* <!-- Contact Section--> */}
          <section className="page-section" id="contact">
      <div className="container">
        <h2 className="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
        <div className="divider-custom">
          <div className="divider-custom-line"></div>
          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
          <div className="divider-custom-line"></div>
        </div>
        <div className="row justify-content-center">
          <div className="col-lg-8 col-xl-7">
            <form id="contactForm" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input className="form-control" id="name" type="text" placeholder="Enter your name..." value={formData.name} onChange={handleChange} required />
                <label htmlFor="name">Full name</label>
              </div>
              <div className="form-floating mb-3">
                <input className="form-control" id="email" type="email" placeholder="name@example.com" value={formData.email} onChange={handleChange} required />
                <label htmlFor="email">Email address</label>
              </div>
              <div className="form-floating mb-3">
                <input className="form-control" id="phone" type="tel" placeholder="(123) 456-7890" value={formData.phone} onChange={handleChange} required />
                <label htmlFor="phone">Phone number</label>
              </div>
              <div className="form-floating mb-3">
                <textarea className="form-control" id="message" placeholder="Enter your message here..." value={formData.message} onChange={handleChange} required></textarea>
                <label htmlFor="message">Message</label>
              </div>
              {successMessage && <div className="text-center mb-3 text-success">Form submission successful!</div>}
              {errorMessage && <div className="text-center mb-3 text-success">Form submission successful!</div>}
              <button className="btn btn-primary btn-xl" id="submitButton" type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </section>
          {/* <!-- Footer--> */}
          {/* <footer className="footer text-center">
            <div className="container">
              <div className="row">
                <!-- Footer Location-->
                <div className="col-lg-4 mb-5 mb-lg-0">
                  <h4 className="text-uppercase mb-4">Location</h4>
                  <p className="lead mb-0">
                    2215 John Daniel Drive
                    <br />
                    Clark, MO 65243
                  </p>
                </div>
                <!-- Footer Social Icons-->
                <div className="col-lg-4 mb-5 mb-lg-0">
                  <h4 className="text-uppercase mb-4">Around the Web</h4>
                  <a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-facebook-f"></i></a>
                  <a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-twitter"></i></a>
                  <a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-linkedin-in"></i></a>
                  <a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-dribbble"></i></a>
                </div>
                <!-- Footer About Text-->
                <div className="col-lg-4">
                  <h4 className="text-uppercase mb-4">About Freelancer</h4>
                  <p className="lead mb-0">
                    Freelance is a free to use, MIT licensed Bootstrap theme created by
                    <a href="http://startbootstrap.com">Start Bootstrap</a>
                    .
                  </p>
                </div>
              </div>
            </div>
          </footer> */}
          {/* <!-- Copyright Section--> */}
          <div className="copyright py-4 text-center text-white">
            <div className="container"><small>Anunkhaporn Kunang</small></div>
          </div>
          {/* <!-- Portfolio Modals-->
        <!-- Portfolio Modal 1--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal1" tabIndex="-1" aria-labelledby="portfolioModal1" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">Buddy Care Web</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-5" src={menu1} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <p className="mb-4">This is a home visit program designed to provide home care services (Home Service), enabling hospitals to automatically refer patients to affiliated hospitals or healthcare centers (RH/RTC) and informing them of the list of referred patients, allowing them to immediately provide home care services to the patients.</p>
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Portfolio Modal 2--> */}
          {/* <div className="portfolio-modal modal fade" id="portfolioModal2" tabIndex="-1" aria-labelledby="portfolioModal2" aria-hidden="true"> */}
          <div className="portfolio-modal modal fade" id="portfolioModal2" tabIndex="-1" aria-labelledby="portfolioModal2" aria-hidden="true" ref={modalRef}>
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">Pharma Care Web</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-5" src={menu2} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <p className="mb-4">The pharmacy management system allows patients who have received treatment to obtain their medication from nearby participating pharmacies.</p>
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Portfolio Modal 3--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal3" tabIndex="-1" aria-labelledby="portfolioModal3" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">One Express Web</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-5" src={menu3} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <p className="mb-4">The food delivery system handles delivery, transportation, and pickup services. It manages the admin system, tracks rider status, and oversees the back-end data management.</p>
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Portfolio Modal 4--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal4" tabIndex="-1" aria-labelledby="portfolioModal4" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">Stretcher Manage Web</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-5" src={menu4} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <p className="mb-4">The porter shift system schedules tasks, records work history, and tracks performance scores of hospital porters.</p>
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Portfolio Modal 5--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal5" tabIndex="-1" aria-labelledby="portfolioModal5" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">Tele-Consulting Web</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-5" src={menu5} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <p className="mb-4">The telemedicine system for factory employees collects data on medical history, treatment follow-ups, and appointments.</p>
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Portfolio Modal 6--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal6" tabIndex="-1" aria-labelledby="portfolioModal6" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">Graphic Design</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-5" src={menu6} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <img className="img-fluid rounded mb-2" src={p1} style={{ width:200, marginRight: '8px' }} alt="..." />
                        <img className="img-fluid rounded mb-2" src={p2} style={{ width:200, marginRight: '8px' }} alt="..." />
                        <img className="img-fluid rounded mb-2" src={p3} style={{ width:200 }} alt="..." />

                        <img className="img-fluid rounded mb-2" src={p4} style={{ width:200, marginRight: '8px' }} alt="..." />
                        <img className="img-fluid rounded mb-2" src={p5} style={{ width:200, marginRight: '8px' }} alt="..." />
                        <img className="img-fluid rounded mb-2" src={p6} style={{ width:200 }} alt="..." />

                        <img className="img-fluid rounded mb-2" src={p7} style={{ width:200, marginRight: '8px' }} alt="..." />
                        <img className="img-fluid rounded mb-2" src={p8} style={{ width:200, marginRight: '8px' }} alt="..." />
                        <img className="img-fluid rounded mb-2" src={p9} style={{ width:200 }} alt="..." />

                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Portfolio Modal 7--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal7" tabIndex="-1" aria-labelledby="portfolioModal7" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">hard skills</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <div className="row justify-content-center" style={{ marginBottom: '5%'}}>
                          <div className="col-lg-12 col-md-12 col-sm-12">
                            <p className="mb-4" style={{ textAlign: 'left', margin: '2px'}}> - Javascript <br/> - HTML <br/> - CSS <br/> - Typescript</p>
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-2" src={s1} style={{ width: 140 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-2" src={s2} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-2" src={s3} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-2" src={s4} style={{ width: 140 }} alt="..." />
                          </div>
                        </div>
                        {/* <!-- Portfolio Modal - Text--> */}
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Portfolio Modal 8--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal8" tabIndex="-1" aria-labelledby="portfolioModal8" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">soft skills</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <img className="img-fluid rounded mb-2" src={menu8} alt="..." />
                        {/* <!-- Portfolio Modal - Text--> */}
                        <p className="mb-4" style={{ textAlign: 'left', margin: '2px'}}>Good <br/> Teamwork<br/> Patience <br/> Logical thinking <br/> Good attitude<br/> Open communication</p>
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Portfolio Modal 9--> */}
          <div className="portfolio-modal modal fade" id="portfolioModal9" tabIndex="-1" aria-labelledby="portfolioModal9" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header border-0"><button className="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div className="modal-body text-center pb-5">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-lg-8">
                        {/* <!-- Portfolio Modal - Title--> */}
                        <h2 className="portfolio-modal-title text-secondary text-uppercase mb-0">Tools&Frameworks</h2>
                        {/* <!-- Icon Divider--> */}
                        <div className="divider-custom">
                          <div className="divider-custom-line"></div>
                          <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                          <div className="divider-custom-line"></div>
                        </div>
                        {/* <!-- Portfolio Modal - Image--> */}
                        <div className="row justify-content-center" style={{ marginBottom: '5%'}}>
                          <div className="col-lg-6 col-md-6 col-sm-12">
                          <p className="mb-4" style={{ textAlign: 'left', margin: '2px'}}>GitLab <br/> Docker <br/> Kubernetes <br/> Trello , Clickup <br/>Visual Studio</p>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-12">
                          <p className="mb-4" style={{ textAlign: 'left', margin: '2px'}}>Vue.js <br/>  Vuex <br/>  Vuetify <br /> Angular <br/>  NG-ZORRO <br /> Node.js</p>
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s5} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s6} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s7} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s8} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s9} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s10} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s11} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s12} style={{ width: 100 }} alt="..." />
                          </div>
                          <div className="col-lg-3 col-md-6 col-sm-12">
                            <img className="img-fluid rounded mb-4" src={s13} style={{ width: 100 }} alt="..." />
                          </div>
                        </div>
                        {/* <!-- Portfolio Modal - Text--> */}
                        
                        <button className="btn btn-primary" data-bs-dismiss="modal">
                          <i className="fas fa-xmark fa-fw"></i>
                          Close Window
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Bootstrap core JS--> */}
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
          {/* <!-- Core theme JS--> */}
          <script src="js/scripts.js"></script>

          <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        </body>
      </html>

      {/* </header> */}
    </div>
  );
}

export default App;